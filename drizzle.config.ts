import { type Config } from "drizzle-kit";

export default {
  schema: "./src/server/db/schema.ts",
  driver: "d1",
  dbCredentials: {
    wranglerConfigPath: "@/wrangler.toml",
    dbName: "solidcode",
  },
  tablesFilter: ["solidcode_*"],
  out: "./src/migrations",
} satisfies Config;
