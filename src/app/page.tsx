import Link from "next/link";
import { getAllProblems } from "@/server/data/problem-dto";
import { type Problem } from "@/server/db/schema";

export default async function HomePage() {
  const problems = await getAllProblems();
  return (
    <main className="p-10">
      <h1>Problems</h1>
      <ProblemList problems={problems} />
    </main>
  );
}

function ProblemList(props: { problems: Problem[] }) {
  const { problems } = props;
  if (problems.length === 0) {
    return <p>We cannot find any problems.</p>;
  }
  return <ul>{problems.map(createListItem)}</ul>;
}

function createListItem(problem: Problem) {
  return (
    <li key={problem.id}>
      <Link href={`/problems/${problem.id}`}>
        {problem.title}
      </Link>
    </li>
  );
}
