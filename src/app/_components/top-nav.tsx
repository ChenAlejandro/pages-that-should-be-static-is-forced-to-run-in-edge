"use client"
import { useTheme } from 'next-themes';
import ThemeToggle from "@/components/ui/theme-toggle";

export function TopNav() {
  const { resolvedTheme, setTheme } = useTheme();

  const toggleCurrentTheme = () =>
    setTheme(resolvedTheme === 'dark' ? 'light' : 'dark');
  return (
    <nav className="flex w-full items-center justify-between border-b p-4 text-xl font-semibold">
      <ThemeToggle onClick={toggleCurrentTheme} />
    </nav>
  );
}
