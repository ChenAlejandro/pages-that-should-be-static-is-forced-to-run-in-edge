import { notFound } from "next/navigation";
import { getProblem } from "@/server/data/problem-dto";

export const runtime = "edge";

export default async function ProblemPage({
  params,
}: {
  params: { id: string };
}) {
  const problemId = parseInt(params.id);
  if (isNaN(problemId)) {
    notFound();
  }
  const problem = await getProblem(problemId);
  if (!problem) {
    notFound();
  }
  return <div>problem: {problem.title}</div>;
}
