import "server-only";
import { db } from "@/server/db";
import { problems } from "@/server/db/schema";

export async function getAllProblems() {
  return db.select().from(problems);
}

export async function getProblem(id: number) {
  return await db.query.problems.findFirst({
    where: (problems, { eq }) => eq(problems.id, id),
  });
}
