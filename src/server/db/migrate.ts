import { migrate } from "drizzle-orm/d1/migrator";
import { drizzle } from "drizzle-orm/d1";
import * as schema from "@/server/db/schema";

const db = drizzle(process.env.SOLID_CODE_DATABASE, { schema });
await migrate(db, { migrationsFolder: "src/migrations" });
