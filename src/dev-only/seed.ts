import { problems } from "@/server/db/schema";
import { drizzle } from "drizzle-orm/d1";
import * as schema from "@/server/db/schema";

const db = drizzle(process.env.SOLID_CODE_DATABASE, { schema });

async function main() {
  const problem1 = {
    id: 1,
    title: "Revertir el arreglo",
    description:
      "Dado un arreglo de números, retornar un arreglo revirtiendo el orden.",
  };
  await db
    .insert(problems)
    .values(problem1)
    .onConflictDoUpdate({ target: problems.id, set: problem1 });
}

main()
  .then(async () => {
    process.exit(0);
  })
  .catch(async (e) => {
    console.error(e);
    process.exit(1);
  });
